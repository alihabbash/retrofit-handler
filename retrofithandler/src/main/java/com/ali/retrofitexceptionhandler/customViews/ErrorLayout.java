package com.ali.retrofitexceptionhandler.customViews;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.util.AttributeSet;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.ali.retrofitexceptionhandler.R;

/**
 * Created by ALI on 8/12/2018.
 */

public class ErrorLayout extends FrameLayout {

    /*Fields*/
    private Context context;
    private AttributeSet attrs;
    private int styleAttr;
    private View view;

    /*components*/
    private ImageView errorImage;
    private TextView errorMessage;
    private TextView errorTitle;
    private Button errorButton;

    /*attributes*/
    private Drawable imageDrawable;
    private int titleTextSize;
    private int titleTextColor;
    private int messageTextSize;
    private int messageTextColor;
    private String titleText;
    private String messageText;
    private Drawable buttonBackgroundDrawable;
    private int buttonTextSize;
    private int buttonTextColor;
    private String buttonText;

    public ErrorLayout(Context context) {
        super(context);
        this.context = context;
        initView();
    }

    public ErrorLayout(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        this.context = context;
        this.attrs = attrs;
        initView();
    }

    public ErrorLayout(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.context = context;
        this.attrs = attrs;
        this.styleAttr = defStyleAttr;
        initView();
    }

    private void initView() {
        this.view = this;
        inflate(context, R.layout.error_layout, this);

        TypedArray typedArray = context.obtainStyledAttributes(attrs, R.styleable.ErrorLayout, styleAttr, 0);
        try {
            imageDrawable = typedArray.getDrawable(R.styleable.ErrorLayout_imageSrc);
            titleTextSize = typedArray.getDimensionPixelSize(R.styleable.ErrorLayout_titleTextSize, 0);
            titleTextColor = typedArray.getColor(R.styleable.ErrorLayout_tileTextColor, 0);
            messageTextSize = typedArray.getDimensionPixelSize(R.styleable.ErrorLayout_messageTextSize, 0);
            messageTextColor = typedArray.getColor(R.styleable.ErrorLayout_messageTextColor, 0);
            titleText = typedArray.getString(R.styleable.ErrorLayout_errorTitle);
            messageText = typedArray.getString(R.styleable.ErrorLayout_errorMessage);
            buttonBackgroundDrawable = typedArray.getDrawable(R.styleable.ErrorLayout_buttonBackground);
            buttonTextSize = typedArray.getDimensionPixelSize(R.styleable.ErrorLayout_buttonTextSize, 0);
            buttonTextColor = typedArray.getColor(R.styleable.ErrorLayout_buttonTextColor, 0);
            buttonText = typedArray.getString(R.styleable.ErrorLayout_buttonText);

            errorImage = findViewById(R.id.error_image);
            errorMessage = findViewById(R.id.error_description);
            errorTitle = findViewById(R.id.error_title);
            errorButton = findViewById(R.id.error_retry_button);

//            if(imageDrawable != null)
//                setImageDrawable(imageDrawable);
//
//            final int sdk = android.os.Build.VERSION.SDK_INT;
//            if (sdk < android.os.Build.VERSION_CODES.JELLY_BEAN)
//                errorButton.setBackgroundDrawable(buttonBackgroundDrawable);
//            else
//                errorButton.setBackground(buttonBackgroundDrawable);
//
//            errorButton.setTextColor(buttonTextColor);
//            errorButton.setTextSize(buttonTextSize);
//            errorButton.setText(buttonText);
//
//            errorTitle.setTextSize(titleTextSize);
//            errorTitle.setTextColor(titleTextColor);
//            errorTitle.setText(titleText);
//            errorMessage.setTextColor(messageTextColor);
//            errorMessage.setTextSize(messageTextSize);
//            errorMessage.setText(messageText);


        } finally {
            typedArray.recycle();
        }
    }

    public void setImageDrawable(Drawable imageDrawable) {
        errorImage.setImageDrawable(imageDrawable);
    }

    public void setTitleTextSize(int titleTextSize) {
        errorTitle.setTextSize(titleTextSize);
    }

    public void setTitleTextColor(int titleTextColor) {
        errorTitle.setTextColor(titleTextColor);
    }

    public void setMessageTextSize(int messageTextSize) {
        errorMessage.setTextSize(messageTextSize);
    }

    public void setMessageTextColor(int messageTextColor) {
        errorMessage.setTextColor(messageTextColor);
    }

    public void setTitleText(String titleText) {
        errorTitle.setText(titleText);
    }

    public void setMessageText(String messageText) {
        errorMessage.setText(messageText);
    }

    public void setButtonBackgroundDrawable(Drawable buttonBackgroundDrawable) {
        final int sdk = android.os.Build.VERSION.SDK_INT;
        if (sdk < android.os.Build.VERSION_CODES.JELLY_BEAN)
            errorButton.setBackgroundDrawable(buttonBackgroundDrawable);
        else
            errorButton.setBackground(buttonBackgroundDrawable);
    }

    public void setButtonTextSize(int buttonTextSize) {
        errorButton.setTextSize(buttonTextSize);
    }

    public void setButtonTextColor(int buttonTextColor) {
        errorButton.setTextColor(buttonTextColor);
    }

    public void setButtonText(String buttonText) {
        errorButton.setText(buttonText);
    }
}
