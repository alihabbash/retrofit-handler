package com.ali.retrofitexceptionhandler.exceptions;

/**
 * Created by ALI on 8/10/2018.
 */

public class ConnectionException extends Exception {
    public ConnectionException() {
    }

    public ConnectionException(String message) {
        super(message);
    }
}
