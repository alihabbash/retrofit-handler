package com.ali.retrofitexceptionhandler.exceptions;

/**
 * Created by ALI on 8/11/2018.
 */

public class BadRequestException extends Exception {
    public BadRequestException() {
    }

    public BadRequestException(String message) {
        super(message);
    }
}
