package com.ali.retrofitexceptionhandler.exceptions;

/**
 * Created by ALI on 8/11/2018.
 */

public class ForbiddenException extends Exception {

    public ForbiddenException() {
    }

    public ForbiddenException(String message) {
        super(message);
    }
}
