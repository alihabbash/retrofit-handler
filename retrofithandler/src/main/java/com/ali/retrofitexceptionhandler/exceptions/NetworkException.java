package com.ali.retrofitexceptionhandler.exceptions;

/**
 * Created by ALI on 8/10/2018.
 */

public class NetworkException extends Exception {

    public NetworkException(String message) {
        super(message);
    }
}
