package com.ali.retrofitexceptionhandler.exceptions;

/**
 * Created by ALI on 8/10/2018.
 */

public class InternalServerException extends Exception {

    public InternalServerException() {
    }

    public InternalServerException(String message) {
        super(message);
    }
}
