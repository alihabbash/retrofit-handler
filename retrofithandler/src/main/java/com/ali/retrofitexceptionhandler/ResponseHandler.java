package com.ali.retrofitexceptionhandler;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.ali.retrofitexceptionhandler.exceptions.BadRequestException;
import com.ali.retrofitexceptionhandler.exceptions.ConnectionException;
import com.ali.retrofitexceptionhandler.exceptions.ForbiddenException;
import com.ali.retrofitexceptionhandler.exceptions.InternalServerException;
import com.ali.retrofitexceptionhandler.exceptions.NetworkException;
import java.io.IOException;
import io.reactivex.Observable;
import io.reactivex.functions.Function;

/**
 * Created by ALI on 8/10/2018.
 */

public class ResponseHandler {

    public static void assertValidResponse(int responseCode, @Nullable Object rootResponseBody, @NonNull String errorBodyMessage) throws Exception {
        if (!(responseCode == 200) || rootResponseBody == null) {
            switch (responseCode) {
//                case 400:
//                    throw new NetworkException(newsResponse.errorDetails().error());
                case 403:
                    throw new ForbiddenException("Forbidden");
                case 500:
                    throw new InternalServerException("Internal Server Error");
                case 502:
                    throw new BadRequestException("Bad Request");
                default:
                    throw new NetworkException(errorBodyMessage);
//                    throw new NetworkException(response.message(), response.code());
            }
        }
    }

    public static <T> Function<Throwable, Observable<T>> transformIOExceptionIntoConnectionException() {
        // if error is IOException then transform it into ConnectionException
        return t -> t instanceof IOException ? Observable.error(new ConnectionException(t.getMessage())) : Observable.error(
                t);
    }
}
