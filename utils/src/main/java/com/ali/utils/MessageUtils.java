package com.ali.utils;

import android.app.Dialog;
import android.content.Context;
import android.support.annotation.IntDef;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.function.Function;

/**
 * Created by ALI on 9/8/2018.
 */

public class MessageUtils {

    public static class Builder {

        @IntDef({ERROR_MESSAGE, SUCCESS_MESSAGE})
        @Retention(RetentionPolicy.SOURCE)
        public @interface Type {
        }

        public static final int ERROR_MESSAGE = 0;
        public static final int SUCCESS_MESSAGE = 1;

        private Context context; //This is important, so we'll pass it to the constructor.
        private int layoutRedId = R.layout.dialog_error;
        private int headerResId = R.id.dialog_title;
        private int messageResId = R.id.dialog_content;
        private int okButtonResId = R.id.bt_close;
        private int messageType = ERROR_MESSAGE;
        private Runnable clickAction = null;
        private String title = "Oops";
        private String message = "Something went wrong";
        private String okButtonText = "OK";

        public Builder(Context context) {
            this.context = context;
        }

        public Builder withType(int messageType) {
            this.messageType = messageType;
            return this;
        }

        public Builder withLayoutResId(int layoutRedId) {
            this.layoutRedId = layoutRedId;
            return this;  //By returning the builder each time, we can create a fluent interface.
        }

        public Builder withHeaderResId(int headerResId) {
            this.headerResId = headerResId;
            return this;
        }

        public Builder withContentResId(int contentResId) {
            this.messageResId = contentResId;
            return this;
        }

        public Builder withOkButtonResId(int okButtonResId) {
            this.okButtonResId = okButtonResId;
            return this;
        }

        public Builder withButtonClickAction(Runnable clickAction) {
            this.clickAction = clickAction;
            return this;
        }

        public Builder withTitle(String title) {
            this.title = title;
            return this;
        }

        public Builder withMessage(String message) {
            this.message = message;
            return this;
        }

        public Builder withOkButtonText(String okButtonText) {
            this.okButtonText = okButtonText;
            return this;
        }

        public void show() {
            //Here we create the actual bank account object, which is always in a fully initialised state when it's returned.
            if (messageType == ERROR_MESSAGE)
                layoutRedId = R.layout.dialog_error;
            if (messageType == SUCCESS_MESSAGE)
                layoutRedId = R.layout.dialog_success;

            final Dialog dialog = new Dialog(context);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setContentView(layoutRedId);

            TextView messageTextView = dialog.findViewById(messageResId);
            messageTextView.setText(message);

            TextView header = dialog.findViewById(headerResId);
            header.setText(title);

            dialog.setCancelable(false);
            WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
            lp.copyFrom(dialog.getWindow().getAttributes());
            lp.width = WindowManager.LayoutParams.WRAP_CONTENT;
            lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
            Button okButton = dialog.findViewById(okButtonResId);
            okButton.setText(okButtonText);
            okButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (clickAction != null)
                        clickAction.run();
                    dialog.dismiss();
                }
            });

            dialog.show();
            dialog.getWindow().setAttributes(lp);
        }
    }

    //Fields omitted for brevity.
    private MessageUtils() {
        //Constructor is now private.
    }
    //Getters and setters omitted for brevity.

}
