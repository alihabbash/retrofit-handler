package com.ali.retrofitexceptionhandlerexample;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.ali.retrofitexceptionhandler.ResponseHandler;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        try {
            ResponseHandler.assertValidResponse(200,"","");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
